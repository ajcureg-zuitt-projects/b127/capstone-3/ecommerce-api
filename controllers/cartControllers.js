const Product = require('../models/Product');
const Cart = require('../models/Cart');


module.exports.createCart = async (data) => {

	let totalPriceOfAll = 0;
	let productDetail = await Product.findById(data.order.productId);
	let isCartExist = await Cart.findOne({userId: data.userId});
	
	if (isCartExist) {
		let productIndex = isCartExist.products.findIndex(product => product.productId == data.order.productId);

		if (productDetail.isActive) {

			if (productIndex > -1) {

				let product = isCartExist.products[productIndex];
				product.quantity = data.order.quantity;
				product.totalPerProduct = product.quantity*productDetail.price;
			}else {

				let addProduct = {
					productId: data.order.productId,
					brandName: productDetail.brandName,
					productName: productDetail.productName,
					quantity: data.order.quantity,
					totalPerProduct: productDetail.price*data.order.quantity
				}
				isCartExist.products.push(addProduct);
			} 
		}else {
			return false;
		}

	for (i=0; i< isCartExist.products.length; i++) {
		totalPriceOfAll += isCartExist.products[i].totalPerProduct;
	}

	isCartExist.totalPrice = totalPriceOfAll 
	isCartExist = await isCartExist.save();
	return true;

	}
	
	else {
		let addProduct = [];

		if (productDetail.isActive) {

			let productJSON = {
				productId: data.order.productId,
				brandName: productDetail.brandName,
				productName: productDetail.productName,
				quantity: data.order.quantity,
				totalPerProduct: productDetail.price*data.order.quantity
			}
			await addProduct.push(productJSON);
			let newCart = new Cart ({
				products: addProduct,
				userId: data.userId,
				totalPrice: productDetail.price*data.order.quantity
			});

			return newCart.save().then((result,isError) => {
				return (isError)? false : true;
			})
		} else {
			return false;
		}
	}
}

module.exports.removeProduct = async (data) => {
	let totalPriceOfAll = 0;
	let insideCart = await Cart.findOne({userId:data.userId})
	
	let productIndex = insideCart.products.findIndex(index => index.productId == data.product.productId);
	if (productIndex > -1) {
		insideCart.products.splice(productIndex,1);
	} else {
		return false;
	}
	for (i=0; i < insideCart.products.length; i++) {
		totalPriceOfAll += insideCart.products[i].totalPerProduct;
	}
	insideCart.totalPrice= totalPriceOfAll;
	return insideCart.save().then((result,isError) => {
		return (isError)? false : true;
	})
}



module.exports.myCart = (data) => {
	return Cart.find({userId: data.userId}).then(result =>{
		return result;
	})
}