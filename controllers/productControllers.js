const Product = require('../models/Product');


module.exports.getAllProducts = () => {
	return Product.find().then(result => {
		return result;
	})
}


module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}


module.exports.createProduct = (data) => {

	return Product.findOne({productName: data.product.productName}).then(result => {
		if (result !== null && result.productName == data.product.productName) {
			return false;
		}else {
			let newProduct = new Product({
				brandName: data.product.brandName,
				productName: data.product.productName,
				description: data.product.description,
				price: data.product.price
			})
			return newProduct.save().then((product,isError) => {
				return (isError)? false : true;
			})
		}
	})
}
	
	

module.exports.modifyProduct = (reqParams,data) => {

	let modifiedProduct = {
			brandName: data.newUpdate.brandName,
			productName: data.newUpdate.productName,
			description: data.newUpdate.description,
			price: data.newUpdate.price,
			isActive: data.newUpdate.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, modifiedProduct).then((course,isError) => {
		return (isError)? false : true;
	})

}
