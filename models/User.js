const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		require: [true, 'First name is required']
	},
	lastName: {
		type: String,
		require: [true, 'Last name is required']
	},
	mobileNumber: {
		type: String,
		require: [true, 'Mobile number is required']
	},
	email: {
		type: String,
		require: [true, 'Email is required']
	},
	password: {
		type: String,
		require: [true, 'Password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
	
})

module.exports = mongoose.model("User", userSchema)