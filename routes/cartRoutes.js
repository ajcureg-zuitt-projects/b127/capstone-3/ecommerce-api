const express = require('express');
const router = express.Router();
const cartControllers = require('../controllers/cartControllers')
const auth = require('../auth');

router.post('/addToCart', auth.verify, (req,res) => {
	const data = {
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}

	if(!data.isAdmin){
		cartControllers.createCart(data).then(result => res.send(result))
	}else {
		res.send(false)
	}

})



router.get('/myCart', (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	if (!data.isAdmin) {
		cartControllers.myCart(data).then(result => res.send(result))
	} else {
		res.send(false);
	}
})



router.put('/removeProduct', (req,res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	} 
	if (!data.isAdmin) {
		cartControllers.removeProduct(data).then(result => res.send(result))
	} else {
		res.send(false)
	}
})


module.exports = router;