const express = require('express');
const router = express.Router();
const orderControllers = require('../controllers/orderControllers')
const auth = require('../auth');

router.post('/createOrder', auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}

	if(!data.isAdmin){
		orderControllers.createOrder(data).then(result => res.send(result))
	}else {
		res.send(false)
	}
})


router.get('/allOrders', auth.verify, (req,res) =>  {
	const data =  {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin) {
		orderControllers.getAllOrders().then(result => res.send(result))
	}else {
		res.send(false);
	}
})


router.get('/myOrders', (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	console.log(data.userId)
	if (!data.isAdmin) {
		orderControllers.myOrders(data).then(result => res.send(result))
	} else {
		res.send(false);
	}
})


module.exports = router;