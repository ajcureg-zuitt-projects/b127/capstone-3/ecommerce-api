const express = require('express');
const mongoose = require('mongoose');
const cors =require('cors');

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');
const cartRoutes = require('./routes/cartRoutes');

const app = express();

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.ogakp.mongodb.net/Batch127_Capstone-3?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	UseUnifiedTopology:true
});

mongoose.connection.once("open", () => console.log ("Now connected to MongoDB Atlas"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/users',userRoutes);
app.use('/products',productRoutes);
app.use('/cart',cartRoutes);
app.use('/orders',orderRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});